public class SwitchCaseExample4{
	public static void main(String args[]){
	int num =10;
	int reminder=num%2;
	switch (reminder){
		case 0:
				System.out.println(num + " is an even number");
				break;
		case 1:
				System.out.println(num +"is an odd number");
				break;
		default:
			System.out.println(num +"is an invalid number");
	}
	}
}
			