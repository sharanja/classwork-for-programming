public class ForLoopDemo4{
	public static void main(String args[]){
		int num = 5;
		double x = Math.pow(num,2);
	
		for(int i = 1;i<=x;i++ ){
	
			System.out.print("* ");
			if(i%num==0){
				System.out.println();
			}
		}
	}
}