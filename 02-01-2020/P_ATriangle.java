public class P_ATriangle{
	public static void main(String args[]){

		int length1 = 6;		 
		int length2 = 5;
		int length3 = 4;
		double height = 8;
		double base = 4;
		double area = 0.5*base*height;
		int perimeter = length1+length2+length3;

		System.out.println("Area of triangle = "+ area);
		System.out.println("Perimeter of triangle = "+ perimeter);

}
}