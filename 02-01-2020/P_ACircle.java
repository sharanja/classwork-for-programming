public class P_ACircle{
	public static void main(String args[]){
		int radius = 7;
		double perimeter = 2*3.14*radius;
		double area = 3.14*radius*radius;
		System.out.println("Perimeter of circle = "+perimeter);
		System.out.println("Area of circle = "+area);
}
}